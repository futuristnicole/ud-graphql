const express = require('express');
// const expressGraphQL =require('express-graphql');
const graphqlHTTP = require('express-graphql');
const schema = require('./schema/schema');


const app = express();

app.get('/', (req, res) => {
    res.send('GraphQL is amazing!');
});

app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true,
}));

app.listen(4000, () => console.log('Running server on port localhost:4000/graphql'));
